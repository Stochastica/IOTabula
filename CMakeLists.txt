cmake_minimum_required(VERSION 3.5)

add_subdirectory(src)

option(TEST "Enable testing")
if (${TEST})
	message("Testing is enabled")
	include(CTest)
	enable_testing()
	add_subdirectory(test)
endif()
