/**
 * This file is a part of the IO Tabula library
 */

#ifndef _IOTABULA_OCSVSTREAM_HPP
#define _IOTABULA_OCSVSTREAM_HPP

#include <ostream>

#include "Row.hpp"

namespace iotab
{

class OCSVStream final
{
public:
	~OCSVStream();

	static OCSVStream writeFile(char const* filename);
	static OCSVStream writeStream(std::ostream*);

	/**
	 * Prints a row to the CSV stream.
	 *
	 * If the cursor is in the middle of a line, the operation  will throw an
	 * exception.
	 */
	OCSVStream& operator<<(Row const&);
	OCSVStream& operator<<(std::string const&);

	OCSVStream& newline();

	void flush();

	/**
	 * Converts a string to a form that can be stored as a CSV entry.
	 *
	 * e.g. 1234IJK -> 1234IJK
	 *      tree",  -> "tree"","
	 *      '"'     -> '""'
	 */
	static std::string escape(std::string const& s);
private:
	OCSVStream(std::ostream*, bool ownership);

	bool streamOwnership_;
	std::ostream* out_;
	bool rowBegin_; // If false, the cursor is in the middle of a line.
};

} // namespace iotab

#endif // !_IOTABULA_OCSVSTREAM_HPP
