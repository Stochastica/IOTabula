#include "Row.hpp"


namespace iotab
{

std::string
Row::toString() const
{
	std::string result;

	for (auto const& s: data_)
		result += "[" + s + "]";

	return result;
}

} // namespace iotab
